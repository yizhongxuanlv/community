# Kernel

openEuler是一个开源、免费的Linux发行版平台，将通过开放的社区形式与全球的开发者共同构建一个开放、多元和架构包容的软件生态体系。同时，openEuler也是一个创新的平台，鼓励任何人在该平台上提出新想法、开拓新思路、实践新方案。
openEule 内核为openEuler提供安全、稳定、可靠的基座。openEuler 内核源于上游 Linux 社区，积极参与贡献上游社区，提倡和鼓励补丁优先合入上游社区。希望在社区的共同努力下，越来越多有益特性合入上游社区。

- 源于社区，贡献社区
- 使能和完善鲲鹏生态构建
- 打造安全、稳定、可靠、高性能的基础设施基座

# 组织会议

- 公开的会议时间：北京时间，每周X 下午，XX点~XX点

待定。

# 成员

- 郭寒军[@guohanjun]
- 谢秀奇[@xiexiuqi]
- 杨颖梁[@yangyingliang]

### Maintainer列表

- 杨颖梁[@yangyingliang]

### Committer列表

Everyone could send patches to openeuler by mailing list.

# 联系方式

- [邮件列表](kernel@openeuler.org)
- [IM](#openeuler-dev)

# 项目清单

项目名称：

repository地址：
- https://gitee.com/openeuler/kernel
- https://gitee.com/src-openeuler/kernel

# 常见问题

[FAQ](./faq.md)